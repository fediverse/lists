# Blockierlisten

## „Threads“ von Meta (Facebook, Instagram, WhatsApp)

Um die Fediverse-Instanz von Meta präventiv zu blockieren, können Mastodon-User wie folgt vorgehen:

1. Die CSV-Datei aus diesem Projekt herunterladen.
2. In den persönlichen Mastodon-Einstellungen auf „Importieren und Exportieren“ gehen, dann auf „Importieren“, d.h. hierhin: https://digitalcourage.social/settings/import
3. In der Auswahlbox „Domain-Sperrliste“ auswählen
4. Die zuvor heruntergeladene CSV-Datei dort hochladen
5. Wenn das geklappt hat, müssten Einträge mit „threads“ auf https://digitalcourage.social/domain_blocks zu sehen sein
